const path = require('path');
const assert = require('assert');
const Config = require('./../src');

describe('node-config', () => {
  const defaultEnv = 'default';
  const failTestEnv = 'test-fail';

  const configPath = path.join(__dirname, 'files');
  const fieldsToIgnore = ['ignoring.property'];

  describe('validation errors', () => {
    it('throws Error for invalid environment', () => {
      try {
        Config.get(null, configPath, fieldsToIgnore);
      } catch (err) {
        assert.ok(err instanceof Error);
        assert.ok(/NODE_ENV/g.test(err.message));
        return;
      }

      throw new Error('Must be throw error');
    });

    it('throws Error for invalid config path', () => {
      try {
        Config.get(defaultEnv, null, fieldsToIgnore);
      } catch (err) {
        assert.ok(err instanceof Error);
        assert.ok(/config/g.test(err.message));
        return;
      }

      throw new Error('Must be throw error');
    });

    [{}, [], ['test-1', 1, 'test-2']].map((toIgnore, i) => {
      it(`throws Error for invalid fieldsToIgnore, #${i + 1}`, () => {
        try {
          Config.get(defaultEnv, configPath, {});
        } catch (err) {
          assert.ok(err instanceof Error);
          assert.ok(/"fieldsToIgnore"/g.test(err.message));
          return;
        }

        throw new Error('Must be throw error');
      });
    });

    it('throws Error for invalid config structure', () => {
      process.env.NODE_ENV = failTestEnv;

      try {
        Config.get(defaultEnv, configPath, fieldsToIgnore);
      } catch (err) {
        delete process.env.NODE_ENV;
        assert.ok(err instanceof Error);
        assert.ok(/Invalid type of param/g.test(err.message));
        return;
      }

      delete process.env.NODE_ENV;
      throw new Error('Must be throw error');
    });
  });

  describe('successfully get config instance', () => {
    it('without fieldsToIgnore', () => {
      return Config.get(defaultEnv, configPath);
    });

    it('with fieldsToIgnore', () => {
      return Config.get(defaultEnv, configPath, fieldsToIgnore);
    });
  });
});
