const path = require('path');
const assert = require('assert');
const Validator = require('./../src/validator');

describe('node-config-validator', () => {
  const fieldsToIgnore = ['ignoring.property'];

  const defaultConfigPath = path.join(__dirname, 'files', 'config.default.json');
  const invalidConfigPath = path.join(__dirname, 'files', 'config.test-fail.json');
  const invalidConfigPathByArrayProp = path.join(__dirname, 'files', 'config.test-fail-1.json');
  const invalidConfigPathByObjectProp = path.join(__dirname, 'files', 'config.test-fail-2.json');

  it('throws Error for invalid config path', () => {
    try {
      return Validator.validate(defaultConfigPath, 'invalid/path/to/config/file');
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/ENOENT/g.test(err.message));
    }
  });

  it('resolve for default config file path, without fieldsToIgnore', () => {
    return Validator.validate(defaultConfigPath, defaultConfigPath);
  });

  it('resolve for default config file path, with fieldsToIgnore', () => {
    return Validator.validate(defaultConfigPath, defaultConfigPath, fieldsToIgnore);
  });

  it('throws Error for invalid config, without fieldsToIgnore', () => {
    try {
      return Validator.validate(defaultConfigPath, invalidConfigPath);
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/Invalid type of param/g.test(err.message));
    }
  });

  it('throws Error for invalid config, with fieldsToIgnore', () => {
    try {
      return Validator.validate(defaultConfigPath, invalidConfigPath, fieldsToIgnore);
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/Invalid type of param/g.test(err.message));
    }
  });

  it('throws Error for invalid config by array property', () => {
    try {
      return Validator.validate(defaultConfigPath, invalidConfigPathByArrayProp, fieldsToIgnore);
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/Invalid type of param "array-property"/g.test(err.message));
    }
  });

  it('throws Error for invalid config by object property', () => {
    try {
      return Validator.validate(defaultConfigPath, invalidConfigPathByObjectProp, fieldsToIgnore);
    } catch (err) {
      assert.ok(err instanceof Error);
      assert.ok(/Invalid type of param "object-property"/g.test(err.message));
    }
  });
});
