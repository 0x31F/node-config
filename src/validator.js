const fs = require('fs');
const isArray = Array.isArray;

let ignoreFields = [];
let isNeedHandleIgnoringFields = false;

module.exports.validate = (defaultFilePath, currentFilePath, defaultEnv, fieldsToIgnore = []) => {
  if (defaultFilePath === currentFilePath) {
    return Promise.resolve();
  }

  if (fieldsToIgnore.length) {
    readIgnoreSettings(fieldsToIgnore);
  }

  const defaultConfig = readJSON(defaultFilePath);
  const currentConfig = readJSON(currentFilePath);

  walk(defaultConfig, currentConfig, null, currentFilePath);

  function walk(defaultConfig, currentConfig, currentPath = '') {
    Object.keys(defaultConfig).forEach(property => {
      const path = currentPath ? `${currentPath}:${property}` : property;
      const base = defaultConfig[property];
      const current = currentConfig[property];
      const weNeedToGoDeeper = !ignoreFields.includes(path) && isObject(base);

      if (isInvalidParam(base, current, path)) {
        throw new Error(`Invalid type of param "${path}" for config "${currentFilePath}". Compare with "config.${defaultEnv}.json"`);
      }

      if (weNeedToGoDeeper) {
        walk(base, current, path);
      }
    });
  }
};

function readIgnoreSettings(fieldsToIgnore) {
  ignoreFields = sanitizeIgnoringFields(fieldsToIgnore);
  isNeedHandleIgnoringFields = Array.isArray(ignoreFields) && ignoreFields.length;
}

function readJSON(filePath) {
  const json = fs.readFileSync(filePath);
  return JSON.parse(json);
}

function sanitizeIgnoringFields(fields) {
  return fields
    .toString()
    .split('\n')
    .map(field => field.replace(/\s+/g, ''))
    .filter(field => !field.includes('#') && field.length);
}

function isInvalidParam(base, current, path) {
  const valid = false;

  if (!isNeedHandleIgnoringFields) {
    return isNotEqualTypes(base, current);
  } else if (!ignoreFields.includes(path)) {
    return isNotEqualTypes(base, current);
  } else {
    return valid;
  }
}

function isNotEqualTypes(base, current) {
  if (String(base) !== 'null' && String(current) === 'null') {
    return true;
  }

  if ((isArray(base) && !isArray(current)) || (!isArray(base) && isArray(current))) {
    return true;
  }

  return typeof base !== typeof current;
}

function isObject(value) {
  return typeof value === 'object' && value !== null;
}
