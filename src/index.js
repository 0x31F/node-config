const path = require('path');
const nconf = require('nconf');
const Validator = require('./validator');

module.exports.get = (defaultEnv, configPath, fieldsToIgnore = []) => {
  if (!defaultEnv) {
    throw new Error('Must be specified default value for NODE_ENV');
  }

  if (!configPath) {
    throw new Error('Must be specified config path');
  }

  if (isInvalidArray(fieldsToIgnore)) {
    throw new Error('Param "fieldsToIgnore" must be array of string values');
  }

  nconf.argv().env();
  nconf.defaults({'NODE_ENV': defaultEnv});

  const env = nconf.get('NODE_ENV');
  const defaultFilePath = path.join(configPath, `config.${defaultEnv}.json`);
  const currentFilePath = path.join(configPath, `config.${env}.json`);

  Validator.validate(defaultFilePath, currentFilePath, defaultEnv, fieldsToIgnore);
  return nconf.file({file: currentFilePath});
};

function isInvalidArray(fieldsToIgnore) {
  return !Array.isArray(fieldsToIgnore) || Boolean(fieldsToIgnore.filter(field => typeof field !== 'string').length);
}
